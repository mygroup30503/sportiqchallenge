package com.SportIQ.Challenge.quiz

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class SportIQApp:Application()