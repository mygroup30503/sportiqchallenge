package com.SportIQ.Challenge.quiz.presentation.screens.quiz

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.staggeredgrid.LazyVerticalStaggeredGrid
import androidx.compose.foundation.lazy.staggeredgrid.StaggeredGridCells
import androidx.compose.foundation.lazy.staggeredgrid.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import com.SportIQ.Challenge.quiz.R
import com.SportIQ.Challenge.quiz.domain.model.QuestionDomain
import com.SportIQ.Challenge.quiz.domain.model.QuizScenario

@Composable
fun QuizScreen(navHostController: NavHostController) {
    val viewModel: QuizScreenViewModel = hiltViewModel()
    val status = viewModel.quizStatus.collectAsState(QuizScenario.Start)
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(Color.White.copy(alpha = 0.1f))
    ) {
        Box(modifier = Modifier.fillMaxWidth()) {
            Image(
                painter = painterResource(id = R.drawable.btn_back),
                contentDescription = null,
                modifier = Modifier
                    .padding(8.dp)
                    .clickable {
                        viewModel.getLastQuestion()
                    }
                    .align(Alignment.CenterStart)
                    .size(32.dp)
            )
            Image(
                painter = painterResource(id = R.drawable.quiz_logo),
                contentDescription = null,
                modifier = Modifier
                    .align(Alignment.Center)
                    .padding(vertical = 8.dp, horizontal = 120.dp)
            )
            Image(
                painter = painterResource(id = R.drawable.btn_menu),
                contentDescription = null,
                modifier = Modifier
                    .padding(8.dp)
                    .clickable {
                        navHostController.popBackStack()
                    }
                    .align(Alignment.CenterEnd)
                    .size(32.dp)
            )
        }
        Box(modifier = Modifier.fillMaxSize()) {
            when (val result = status.value) {
                is QuizScenario.Start -> {
                    CircularProgressIndicator(color = Color.White)
                }

                is QuizScenario.Result -> {
                    QuizResult(result = result.result) {
                        navHostController.popBackStack()
                    }
                }

                is QuizScenario.Question -> {
                    QuizQuestion(
                        rightAnswer = viewModel.indexOfRightAnswer,
                        incorrectAnswer = viewModel.indexOfIncorrectAnswer,
                        questionDomain = result.questionItem,
                        onChoseAnswer = {
                            viewModel.onChoseAnswer(it)
                        },
                        onGoNext = {
                            viewModel.getNextQuestion()
                        }
                    )
                }
            }
        }
    }
}

@Composable
fun QuizQuestion(
    rightAnswer: MutableState<Int?>,
    incorrectAnswer: MutableState<Int?>,
    questionDomain: QuestionDomain,
    onChoseAnswer: (Int) -> Unit,
    onGoNext: () -> Unit
) {
    Column(
        modifier = Modifier
            .padding(horizontal = 16.dp)
            .fillMaxSize(),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Card(
            modifier = Modifier
                .padding(4.dp)
                .fillMaxWidth()
                .weight(2f),
            shape = RoundedCornerShape(8.dp),
            border = BorderStroke(2.dp, Color.Black),
            colors = CardDefaults.cardColors(containerColor = Color.Black.copy(alpha = 0.5f))
        ) {
            Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
                Text(
                    text = questionDomain.question,
                    color = Color.White,
                    fontWeight = FontWeight.Bold,
                    textAlign = TextAlign.Center,
                    fontSize = 20.sp
                )
            }
        }
        questionDomain.imageId?.let {
            Card(
                modifier = Modifier
                    .padding(4.dp)
                    .fillMaxWidth()
                    .weight(2f),
                shape = RoundedCornerShape(8.dp),
                border = BorderStroke(2.dp, Color.White)
            ) {
                Image(
                    painter = painterResource(id = it),
                    contentDescription = null,
                    contentScale = ContentScale.Crop,
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(400.dp)
                )
            }
        }

        LazyVerticalStaggeredGrid(
            columns = StaggeredGridCells.Fixed(2),
            modifier = Modifier
                .padding(4.dp)
                .weight(2f),
            verticalItemSpacing = 10.dp
        ) {
            items(questionDomain.answerList) { question ->
                val index = questionDomain.answerList.indexOf(question)
                Card(
                    modifier = Modifier
                        .padding(2.dp)
                        .fillMaxWidth()
                        .height(90.dp)
                        .clickable {
                            onChoseAnswer(index)
                        },
                    shape = RoundedCornerShape(8.dp),
                    border = BorderStroke(2.dp, Color.Black),
                    colors = CardDefaults.cardColors(
                        containerColor = when (index) {
                            rightAnswer.value -> Color.Green
                            incorrectAnswer.value -> Color.Red
                            else -> Color.Black.copy(alpha = 0.5f)
                        }
                    )
                ) {
                    Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
                        Text(
                            text = question, color = Color.White,
                            textAlign = TextAlign.Center,
                            fontWeight = FontWeight.Bold,
                            modifier = Modifier
                                .padding(horizontal = 4.dp)
                                .fillMaxWidth()
                        )
                    }
                }
            }
        }
        Button(
            onClick = { onGoNext.invoke() },
            colors = ButtonDefaults.buttonColors(
                Color.White.copy(alpha = 0.5f)
            ),
            border = BorderStroke(2.dp, Color.Black),
            shape = RoundedCornerShape(8.dp),
            modifier = Modifier
                .padding(4.dp)
                .weight(0.5f)
        ) {
            Text(
                text = "Next",
                color = Color.Black,
                modifier = Modifier.padding(horizontal = 16.dp)
            )
        }
    }
}

@Composable
fun QuizResult(result: Int, onBack: () -> Unit) {
    Box(modifier = Modifier.fillMaxSize()) {
        Card(
            modifier = Modifier
                .padding(8.dp)
                .fillMaxWidth()
                .height(300.dp)
                .align(Alignment.Center),
            shape = RoundedCornerShape(8.dp),
            border = BorderStroke(2.dp, Color.Black),
            colors = CardDefaults.cardColors(containerColor = Color.White.copy(alpha = 0.5f))
        ) {
            Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
                Text(
                    text = "Your Score\n\n$result/10\n\nNot Bad",
                    color = Color.Black,
                    fontWeight = FontWeight.Bold,
                    modifier = Modifier.fillMaxWidth(),
                    fontSize = 30.sp,
                    textAlign = TextAlign.Center
                )
            }
        }
        Button(
            onClick = { onBack.invoke() },
            colors = ButtonDefaults.buttonColors(
                Color.White.copy(alpha = 0.5f)
            ),
            border = BorderStroke(2.dp, Color.Black),
            shape = RoundedCornerShape(8.dp),
            modifier = Modifier.align(Alignment.BottomCenter)
        ) {
            Text(
                text = "To menu",
                color = Color.Black,
                modifier = Modifier.padding(horizontal = 16.dp)
            )
        }
    }

}