package com.SportIQ.Challenge.quiz.di

import com.SportIQ.Challenge.quiz.data.storage.QuestionRepository
import com.SportIQ.Challenge.quiz.data.storage.implementation.QuestionStorage
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
object SICModule {
    @Provides
    fun providedBMEPlayerRepository(): QuestionRepository {
        return QuestionStorage()
    }
}