package com.SportIQ.Challenge.quiz.domain.usecase

import com.SportIQ.Challenge.quiz.data.model.QuestionItem
import com.SportIQ.Challenge.quiz.data.storage.QuestionRepository
import com.SportIQ.Challenge.quiz.domain.model.QuestionDomain
import com.SportIQ.Challenge.quiz.domain.model.QuizScenario
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

class QuizUseCase@Inject constructor(
    private val repository: QuestionRepository
) {
    private var count = 0

    private var rightAnswer = 0

    private val scope = CoroutineScope(Dispatchers.IO)
    private val _status = MutableStateFlow<QuizScenario>(QuizScenario.Start)
    val status: StateFlow<QuizScenario> = _status

    fun answerTheQuestion(index: Int): Int?{
        return if (index==rightAnswer) {
            count++
            null
        } else rightAnswer
    }

    fun getNextQuestion(){
        scope.launch {
            val questionItem = repository.getNextItem()
            if (questionItem!=null) {
                val question = fromDataModelToDomain(questionItem)
                _status.emit(QuizScenario.Question(question))
                rightAnswer = question.indexOfRightQuestion
            }
            else _status.emit(QuizScenario.Result(count))
        }
    }

    fun getLastQuestion(){
        scope.launch {
            val questionItem = repository.getLastItem()
            if (questionItem!=null) {
                val question = fromDataModelToDomain(questionItem)
                _status.emit(QuizScenario.Question(question))
                rightAnswer = question.indexOfRightQuestion
            }
        }
    }

    private fun fromDataModelToDomain(dataQ: QuestionItem): QuestionDomain{
        val list = listOf(dataQ.answer1,dataQ.answer2,dataQ.answer3,dataQ.rightAnswer).shuffled()
        val index = list.indexOf(dataQ.rightAnswer)
        return QuestionDomain(dataQ.question, dataQ.imageId, list, index)
    }
}