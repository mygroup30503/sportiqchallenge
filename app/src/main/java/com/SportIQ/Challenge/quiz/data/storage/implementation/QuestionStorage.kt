package com.SportIQ.Challenge.quiz.data.storage.implementation

import com.SportIQ.Challenge.quiz.R
import com.SportIQ.Challenge.quiz.data.model.QuestionItem
import com.SportIQ.Challenge.quiz.data.storage.QuestionRepository

class QuestionStorage: QuestionRepository {
    override fun getNextItem(): QuestionItem? {
        count++
        return try {
            questionList[count]
        } catch (e: ArrayIndexOutOfBoundsException) {
            null
        }
    }

    override fun getLastItem(): QuestionItem? {
        count--
        return try {
            questionList[count]
        } catch (e: ArrayIndexOutOfBoundsException) {
            count++
            null
        }
    }

    private var count = -1

    private val questionList = listOf(
        QuestionItem(
            question = "A glove worn by Muhammad Ali when he fought Henry Cooper in 1963 is being sold at auction and the owner hopes it will fetch £500,000. Why is the glove so famous – and so expensive?",
            imageId = R.drawable.q1,
            rightAnswer = "It tore during the fight and needed to be replaced, apparently giving Ali precious time to recover when he was in trouble",
            answer1 = "It is the glove with which Ali achieved the first knockout of his career",
            answer2 = "Ali signed the glove “Cassius Clay” after the fight – the last time he used that name in public",
            answer3 = "The glove was lost for 50 years and grew in fame while missing. It was eventually found by a dog called Pickles"
        ),
        QuestionItem(
            question = "What do these six football matches have in common: the League Cup finals of 2019, 2022 and 2024, and the FA Cup finals of 2020, 2021 and 2022?",
            imageId = null,
            rightAnswer = "Chelsea lost them all",
            answer1 = "They were all 0-0 at full time",
            answer2 = "They were all contested by Liverpool and Chelsea",
            answer3 = "Liverpool won them all"
        ),
        QuestionItem(
            question = "The Formula One season begins in Bahrain this weekend. It will be the longest in the history of the sport, with how many races?",
            imageId = R.drawable.q3,
            rightAnswer = "24",
            answer1 = "19",
            answer2 = "21",
            answer3 = "26"
        ),
        QuestionItem(
            question = "Erling Haaland scored five goals for Manchester City in their 6-2 win over Luton this week. Who was the last player to score six goals in an FA Cup match?",
            imageId = null,
            rightAnswer = "George Best",
            answer1 = "Alan Shearer",
            answer2 = "Dion Dublin",
            answer3 = "Luther Blissett"
        ),
        QuestionItem(
            question = "Blackburn Rovers were knocked out of the FA Cup by Newcastle this week. What did Blackburn do in the Cup in the 1880s that has never been done since?",
            imageId = R.drawable.q5,
            rightAnswer = "They won the Cup three years in a row",
            answer1 = "They won the FA Cup by winning every round on penalties",
            answer2 = "They won an FA Cup final 10-0",
            answer3 = "They won the FA Cup on the toss of a coin"
        ),
        QuestionItem(
            question = "Wigan won the World Club Challenge on Saturday night. It’s the fifth time they have won the trophy, matching a record set by which club?",
            imageId = null,
            rightAnswer = "Sydney Roosters",
            answer1 = "St Helens",
            answer2 = "Brisbane Broncos",
            answer3 = "Leeds Rhinos"
        ),
        QuestionItem(
            question = "The NRL season kicks off this weekend with a double-header of Sydney Roosters v Brisbane Broncos and Manly Warringah Sea Eagles v South Sydney Rabbitohs. Where will the games be played?",
            imageId = R.drawable.q7,
            rightAnswer = "Las Vegas",
            answer1 = "London",
            answer2 = "Lisbon",
            answer3 = "Liverpool"
        ),
        QuestionItem(
            question = "Why have the women's cycling team Cynisca been fined and suspended?",
            imageId = null,
            rightAnswer = "They were caught letting air out of rival teams' tyres",
            answer1 = "They put urine into a rival's water bottle",
            answer2 = "They dressed up one of their mechanics to pose as a cyclist so they could take part in a race",
            answer3 = "They were caught dropping nails on to the road before a race"
        ),
        QuestionItem(
            question = "Spain won the Women’s Nations League this week. Who did they beat in the final?",
            imageId = R.drawable.q9,
            rightAnswer = "France",
            answer1 = "England",
            answer2 = "Portugal",
            answer3 = "Sweden"
        ),
        QuestionItem(
            question = "The Hearts player Lawrence Shankland scored a penalty against Hibs on Wednesday night. What happened next?",
            imageId = null,
            rightAnswer = "A Hibs fan threw a pie at him, he took a bite of it and then threw it back",
            answer1 = "He ran off the pitch and left the ground after hearing his wife had gone into labour",
            answer2 = "He broke down in tears as he is a diehard Hibs fan",
            answer3 = "He told the referee he had touched the ball twice so the goal should be ruled out"
        ),

    )
}