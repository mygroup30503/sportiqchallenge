package com.SportIQ.Challenge.quiz.domain.model

data class QuestionDomain(
    val question: String,
    val imageId: Int?,
    val answerList: List<String>,
    val indexOfRightQuestion: Int
)
