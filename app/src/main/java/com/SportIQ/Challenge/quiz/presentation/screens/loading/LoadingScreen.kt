package com.SportIQ.Challenge.quiz.presentation.screens.loading

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import com.SportIQ.Challenge.quiz.presentation.navigation.Screen
import kotlinx.coroutines.delay

@Composable
fun LoadingScreen(navHostController: NavHostController) {
    LaunchedEffect(key1 = true) {
        delay((0..2000L).random())
        navHostController.navigate(Screen.StartScreen.route) {
            popUpTo(Screen.LoadingScreen.route) { inclusive = true }
        }
    }
    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(Color.Black.copy(alpha = 0.5f)),
        contentAlignment = Alignment.Center
    ) {
        Column(horizontalAlignment = Alignment.CenterHorizontally) {
            Text(text = "Loading", color = Color.White, fontWeight = FontWeight.Bold, fontSize = 34.sp)
            Spacer(modifier = Modifier.height(8.dp))
            CircularProgressIndicator(color = Color.White)
        }
    }
}