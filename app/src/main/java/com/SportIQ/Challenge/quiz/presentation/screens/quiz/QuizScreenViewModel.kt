package com.SportIQ.Challenge.quiz.presentation.screens.quiz

import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import com.SportIQ.Challenge.quiz.domain.model.QuestionDomain
import com.SportIQ.Challenge.quiz.domain.usecase.QuizUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class QuizScreenViewModel@Inject constructor(
    private val useCase: QuizUseCase
): ViewModel() {
    val indexOfRightAnswer = mutableStateOf<Int?>(null)
    val indexOfIncorrectAnswer = mutableStateOf<Int?>(null)
    val quizStatus = useCase.status
    fun onChoseAnswer(index: Int){
        val result = useCase.answerTheQuestion(index)
        if (result==null) indexOfRightAnswer.value = index
        else {
            indexOfIncorrectAnswer.value = index
            indexOfRightAnswer.value = result
        }
    }
    fun getNextQuestion(){
        indexOfIncorrectAnswer.value = null
        indexOfRightAnswer.value = null
        useCase.getNextQuestion()
    }

    fun getLastQuestion(){
        indexOfIncorrectAnswer.value = null
        indexOfRightAnswer.value = null
        useCase.getLastQuestion()
    }

    init {
        useCase.getNextQuestion()
    }
}