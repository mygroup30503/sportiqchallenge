package com.SportIQ.Challenge.quiz.data.model

data class QuestionItem(
    val question: String,
    val imageId: Int?,
    val rightAnswer: String,
    val answer1: String,
    val answer2: String,
    val answer3: String,
)