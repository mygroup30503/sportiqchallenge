package com.SportIQ.Challenge.quiz.presentation.navigation


import androidx.compose.runtime.Composable
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.SportIQ.Challenge.quiz.presentation.screens.loading.LoadingScreen
import com.SportIQ.Challenge.quiz.presentation.screens.menu.StartScreen
import com.SportIQ.Challenge.quiz.presentation.screens.quiz.QuizScreen


@Composable
fun MainNavigationGraph() {
    val navHostController = rememberNavController()
    NavHost(
        navController = navHostController, startDestination = Screen.LoadingScreen.route
    ) {
        composable(Screen.LoadingScreen.route){
            LoadingScreen(navHostController = navHostController)
        }
        composable(Screen.StartScreen.route){
            StartScreen(navHostController = navHostController)
        }
        composable(Screen.QuizScreen.route){
            QuizScreen(navHostController = navHostController)
        }
    }
}
