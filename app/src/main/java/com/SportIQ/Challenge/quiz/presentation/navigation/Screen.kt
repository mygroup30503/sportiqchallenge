package com.SportIQ.Challenge.quiz.presentation.navigation

sealed class Screen(val route: String) {
    data object StartScreen: Screen("start_screen")
    data object LoadingScreen: Screen("loading_screen")
    data object QuizScreen: Screen("quiz_screen")
}
