package com.SportIQ.Challenge.quiz.domain.model

import com.SportIQ.Challenge.quiz.data.model.QuestionItem

sealed class QuizScenario {
    data object Start: QuizScenario()
    data class Question(val questionItem: QuestionDomain): QuizScenario()
    data class Result(val result: Int): QuizScenario()
}