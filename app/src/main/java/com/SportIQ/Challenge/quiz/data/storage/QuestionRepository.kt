package com.SportIQ.Challenge.quiz.data.storage

import com.SportIQ.Challenge.quiz.data.model.QuestionItem

interface QuestionRepository {
    fun getNextItem(): QuestionItem?
    fun getLastItem(): QuestionItem?
}